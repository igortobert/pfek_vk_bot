import "reflect-metadata"
import { createConnection, getRepository } from "typeorm";
import { Users } from "./entity/users.entity";
import { Rasps } from "./entity/rasp.entity";
import VkBot from 'node-vk-bot-api';
import Markup from 'node-vk-bot-api/lib/markup';
import Session from 'node-vk-bot-api/lib/session';
import Stage from 'node-vk-bot-api/lib/stage';
import Scene from 'node-vk-bot-api/lib/scene';
import moment from 'moment';
import { start_parce } from "./worker";
import { Config } from "./entity/config.entity";
import { TextDecoder } from "util";
export const root = 20218921
const testToken = '3aeebafda1f16884d9ea69a22ab79766d65a7f3761dd0a8a392d37fd4ffb8a7c8538c3a302e4c5d418454'
const testGroup_id = 122941495
const pfekToken = 'c8dcd3e5ffd68ffe39d629a426503bdbe64168138032d3b14493a962bdd83aa94b5c626745a27358bc6aa'
const pfekGroup_id = 120350742
// export const bot = new VkBot({ token: testToken, group_id: testGroup_id })
export const bot = new VkBot({ token: pfekToken, group_id: pfekGroup_id })
const session = new Session()
export let User = null
export let Rasp = null
export let Conf = null
createConnection().then(async connection => {
    User = await getRepository(Users)
    Rasp = await getRepository(Rasps)
    Conf = await getRepository(Config)
}, error => console.log("Cannot connect: ", error));



const setGroupScene = new Scene('setGroup', async (ctx) => {
    let groupArray = await getAllGroupsNames()
    let key = []
    let row = []
    groupArray.forEach((g, i) => {
        if (i + 1 < 4) {
            row.push(Markup.button(g, 'primary'))
            if (i + 1 === 3) {
                key.push(row)
                row = []
            }
        } else if (i + 1 < groupArray.length + 1) {
            row.push(Markup.button(g, 'primary'))
            if (i + 1 === groupArray.length) {
                row.push(Markup.button('Отмена', 'negative'))
                key.push(row)
            }
        }
    })
    let startKeyboard = Markup.keyboard(
        key
        // [
        // [
        //     Markup.button('БД', 'primary'),
        //     Markup.button('Ф', 'primary'),
        //     Markup.button('СД', 'primary'),
        // ],
        // [
        //     Markup.button('ПОСО', 'primary'),
        //     Markup.button('ЭБУ', 'primary'),
        //     Markup.button('Отмена', 'negative'),
        // ],]
    ).oneTime()
    ctx.scene.next()
    ctx.reply('Выбери свою специальность кнопкой внизу или пришли Отмена для отмены действия:', null, startKeyboard)
}, async (ctx) => {
    if (ctx.message.text === 'отмена' || ctx.message.text === 'Отмена') {
        ctx.scene.leave()
        ctx.reply('Отмена, дак отмена')
        showMainKeyboard(ctx.message.from_id)
    } else {
        let groupNumbersArray = await getAllGroupsNumbers(ctx.message.text)
        let like = ''
        groupNumbersArray.forEach((n, i) => {
            if (i + 1 === groupNumbersArray.length) {
                like += `${n}`
            } else {
                like += `${n}, `
            }
        })
        let keyboard = Markup.keyboard([[Markup.button('Отмена', 'negative'),],]).oneTime()
        ctx.session.group = ctx.message.text.toUpperCase()
        ctx.scene.next()
        ctx.reply(`Напиши цифрами номер своей группы из преложенных ниже или пришли Отмена для отмены действия:\n${like}`, null, keyboard)
    }
}, async (ctx) => {
    if (ctx.message.text === 'отмена' || ctx.message.text === 'Отмена') {
        ctx.scene.leave()
        ctx.reply('Отмена, дак отмена')
        showMainKeyboard(ctx.message.from_id)
    } else {
        ctx.session.course = ctx.message.text
        let gn = `${ctx.session.group} ${ctx.session.course}`
        let groupFound = await Rasp.createQueryBuilder('r')
            .select('r.group_name')
            .where('r.group_name = :gn', { gn })
            .getOne()
        if (groupFound) {
            const vkid = ctx.message.from_id
            let { group_name: group } = groupFound
            let u = await User.findOne({ where: { vkid } })
            if (!u) {
                await User.createQueryBuilder('u').insert().values([{ vkid, group }]).execute()
            } else {
                await User.createQueryBuilder('u').update().set({ group }).where('vkid = :vkid', { vkid }).execute()
            }
            ctx.scene.leave()
            showMainKeyboard(vkid)
            ctx.reply(`Твоя группа: ${ctx.session.group} ${ctx.session.course} \nЗаписал\nТеперь ты можешь выбрать кнопками ниже \nРасп. на сегодня или \nРасп. на завтра\nа если у тебя нет кнопок то \nрс\nили\nрз`)
        } else {
            ctx.scene.selectStep(1)
            let groupArray = await getAllGroupsNames()
            let key = []
            let row = []
            groupArray.forEach((g, i) => {
                if (i + 1 < 4) {
                    row.push(Markup.button(g, 'primary'))
                    if (i + 1 === 3) {
                        key.push(row)
                        row = []
                    }
                } else if (i + 1 < groupArray.length + 1) {
                    row.push(Markup.button(g, 'primary'))
                    if (i + 1 === groupArray.length) {
                        row.push(Markup.button('Отмена', 'negative'))
                        key.push(row)
                    }
                }
            })

            let againKeyboard = Markup.keyboard(key
                // [
                // [
                //     Markup.button('БД', 'primary'),
                //     Markup.button('Ф', 'primary'),
                //     Markup.button('СД', 'primary'),
                // ],
                // [
                //     Markup.button('ПОСО', 'primary'),
                //     Markup.button('ЭБУ', 'primary'),
                //     Markup.button('Отмена', 'negative'),
                // ],]
            ).oneTime()
            ctx.reply('Странно... Я не нашел такую группу. Она правильная? Попробуй еще раз указать ее', null, againKeyboard)
        }
    }

})

const sendAllScene = new Scene('sendAll', async (ctx) => {
    ctx.scene.next()
    ctx.reply('Что будем рассылать?')
}, async (ctx) => {
    if (ctx.message.text === 'отмена' || ctx.message.text === 'Отмена') {
        ctx.scene.leave()
        ctx.reply('Отмена, дак отмена')
        showMainKeyboard(ctx.message.from_id)
    } else {
        let allUsers = await User.find({ select: ['vkid'], where: { vk_group: 'pfek' } })
        if (allUsers) {
            let count = 0
            allUsers.forEach(async (u) => {
                bot.sendMessage(u.vkid, ctx.message.text)
                count++
                if (count === allUsers.length) {
                    ctx.scene.leave()
                    ctx.reply('Сообщения отправлены')
                }
            })
        }
    }
})

const rapsOnDateScene = new Scene('rapsOnDate', async (ctx) => {
    ctx.scene.next()
    ctx.reply('На какую дату нужно расписание?\nнапример: 30.10.2018 (точки как разделитель)')
}, async (ctx) => {
    if (ctx.message.text === 'отмена' || ctx.message.text === 'Отмена') {
        ctx.scene.leave()
        ctx.reply('Отмена, дак отмена')
        showMainKeyboard(ctx.message.from_id)
    } else {
        let vkid = ctx.message.from_id
        let date = ctx.message.text
        updateUserActivity(vkid)
        let { state } = await Conf.findOne({ select: ['state'], where: { param: 'techWоrks' } })
        if (!state) {
            let vkid = ctx.message.from_id
            let u = await User.findOne({ select: ['group'], where: { vkid } })
            if (u) {
                const response = await getRasp(date, u.group)
                if (response.status === 200) {
                    ctx.reply(response.msg)
                } else {
                    ctx.reply(response.msg)
                }
            } else {
                ctx.scene.enter('setGroup')
            }
        } else {
            ctx.reply('Проводятся технические работы\nКак только они закончатся я тебе автоматически его пришлю)')
            await User.update({ vkid }, { wont_rasp: true })
        }
        ctx.scene.leave()
    }
})


const sendByGroupScene = new Scene('sendByGroup', async (ctx) => {
    ctx.scene.next()
    ctx.reply('Для какой группы рассыылка?')
}, async (ctx) => {
    if (ctx.message.text === 'отмена' || ctx.message.text === 'Отмена') {
        ctx.scene.leave()
        ctx.reply('Отмена, дак отмена')
        showMainKeyboard(ctx.message.from_id)
    } else {
        let group = ctx.message.text
        let allUsers = await User.find({ select: ['vkid'], where: { group, vk_group: 'pfek' } })
        if (allUsers) {
            ctx.session.allUsers = allUsers
            ctx.session.group = group
            ctx.scene.next()
            ctx.reply('Какое сообщение разослать?')
        } else {
            ctx.scene.leave()
            ctx.reply('Группа не найдена')
        }
    }
}, async (ctx) => {
    if (ctx.message.text === 'отмена' || ctx.message.text === 'Отмена') {
        ctx.scene.leave()
        ctx.reply('Отмена, дак отмена')
        showMainKeyboard(ctx.message.from_id)
    } else {
        let allUsers = ctx.session.allUsers
        if (allUsers) {
            let count = 0
            allUsers.forEach(async (u) => {
                bot.sendMessage(u.vkid, ctx.message.text)
                // bot.sendMessage(root, ctx.message.text)
                count++
                if (count === allUsers.length) {
                    ctx.scene.leave()
                    ctx.reply(`Сообщение для группы ${ctx.session.group} отправлены`)
                }
            })
        }
    }
})

const stage = new Stage(setGroupScene, sendAllScene, sendByGroupScene, rapsOnDateScene)
bot.use(session.middleware())
bot.use(stage.middleware())

bot.command('РассылкаВГруппу', async (ctx) => {
    if (ctx.message.from_id === root) {
        ctx.scene.enter('sendByGroup')
    }
})

bot.command('Рассылка', async (ctx) => {
    if (ctx.message.from_id === root) {
        ctx.scene.enter('sendAll')
    }
})

bot.command('Парси', async (ctx) => {
    if (ctx.message.from_id === root) {
        await start_parce()
        ctx.reply('Готово!')
    }
})

bot.command('Указать группу', async (ctx) => {
    ctx.scene.enter('setGroup')
})

bot.command('Расп. на завтра', async (ctx) => {
    let vkid = ctx.message.from_id
    updateUserActivity(vkid)
    let { state } = await Conf.findOne({ select: ['state'], where: { param: 'techWоrks' } })
    if (!state) {
        let date
        let today = moment().format('dddd')
        if (today === 'Friday') {
            date = moment().add(3, 'd').format('DD.MM.YYYY')
        } else if (today === 'Saturday') {
            date = moment().add(2, 'd').format('DD.MM.YYYY')

        } else {
            date = moment().add(1, 'd').format('DD.MM.YYYY');
        }

        let vkid = ctx.message.from_id
        let u = await User.findOne({ select: ['group'], where: { vkid, vk_group: 'pfek' } })
        if (u) {
            const response = await getRasp(date, u.group)
            if (response.status === 200) {
                ctx.reply(response.msg)
            } else {
                ctx.reply(response.msg)
            }
        } else {
            ctx.scene.enter('setGroup')
        }
    } else {
        ctx.reply('Проводятся технические работы\nКак только они закончатся я тебе напишу)')
        await User.update({ vkid }, { wont_rasp: true })
    }
})
bot.command('рз', async (ctx) => {
    let vkid = ctx.message.from_id
    updateUserActivity(vkid)
    let { state } = await Conf.findOne({ select: ['state'], where: { param: 'techWоrks' } })
    // let state = false
    if (!state) {
        let date
        let today = moment().format('dddd')
        if (today === 'Friday') {
            date = moment().add(3, 'd').format('DD.MM.YYYY')
        } else if (today === 'Saturday') {
            date = moment().add(2, 'd').format('DD.MM.YYYY')

        } else {
            date = moment().add(1, 'd').format('DD.MM.YYYY');
        }

        let vkid = ctx.message.from_id
        let u = await User.findOne({ select: ['group'], where: { vkid, vk_group: 'pfek' } })
        if (u) {
            const response = await getRasp(date, u.group)
            if (response.status === 200) {
                ctx.reply(response.msg)
            } else {
                ctx.reply(response.msg)
            }
        } else {
            ctx.scene.enter('setGroup')
        }
    } else {
        ctx.reply('Проводятся технические работы\nКак только они закончатся я тебе напишу)')
        await User.update({ vkid }, { wont_rasp: true })
    }
})

bot.command('Расп. на сегодня', async (ctx) => {
    let vkid = ctx.message.from_id
    updateUserActivity(vkid)
    let { state } = await Conf.findOne({ select: ['state'], where: { param: 'techWоrks' } })
    if (!state) {
        let date = moment()
            .format('DD.MM.YYYY')
        let vkid = ctx.message.from_id
        let u = await User.findOne({ select: ['group'], where: { vkid } })
        if (u) {
            const response = await getRasp(date, u.group)
            if (response.status === 200) {
                ctx.reply(response.msg)
            } else {
                ctx.reply(response.msg)
            }
        } else {
            ctx.scene.enter('setGroup')
        }
    } else {
        ctx.reply('Проводятся технические работы\nКак только они закончатся я тебе автоматически его пришлю)')
        await User.update({ vkid }, { wont_rasp: true })
    }
})
bot.command('Расписание на дату', async (ctx) => {
    ctx.scene.enter('rapsOnDate')
})

bot.command('рс', async (ctx) => {
    let vkid = ctx.message.from_id
    updateUserActivity(vkid)
    let { state } = await Conf.findOne({ select: ['state'], where: { param: 'techWоrks' } })
    if (!state) {
        let date = moment()
            .format('DD.MM.YYYY')
        let vkid = ctx.message.from_id
        let u = await User.findOne({ select: ['group'], where: { vkid } })
        if (u) {
            const response = await getRasp(date, u.group)
            if (response.status === 200) {
                ctx.reply(response.msg)
            } else {
                ctx.reply(response.msg)
            }
        } else {
            ctx.scene.enter('setGroup')
        }
    } else {
        ctx.reply('Проводятся технические работы\nКак только они закончатся я тебе автоматически его пришлю)')
        await User.update({ vkid }, { wont_rasp: true })
    }
})

//Начать
bot.command('Начать', async (ctx) => {
    let vkid = ctx.message.from_id
    let u = await User.findOne({ where: { vkid } })
    if (u && u.group) {
        showMainKeyboard(vkid)
    } else {
        ctx.scene.enter('setGroup')
    }
})

bot.command('До скорой встречи, до скорой встречи', async (ctx) => {
    let vkid = ctx.message.from_id
    await User.delete({ vkid })
    ctx.reply('&#128546; Моя любовь\nК тебе навечно &#10084;', null, null, null)
})

bot.command('Мой айди', async (ctx) => {
    let vkid = ctx.message.from_id
    ctx.reply('Твой id: ' + vkid)
})

bot.command('Тех работы', async (ctx) => {
    if (ctx.message.from_id === root) {
        let param = 'techWоrks'
        let techWOrks = await Conf.findOne({ where: { param } })
        if (techWOrks) {
            let bool = !techWOrks.state
            await Conf.update(techWOrks.id, { state: bool })
            ctx.reply(`Технические работы: ${bool}`)
            if (!bool) {
                let date = ''
                let today = moment().format('dddd')
                if (today === 'Friday') {
                    date = moment().add(3, 'd').format('DD.MM.YYYY')
                } else if (today === 'Saturday') {
                    date = moment().add(2, 'd').format('DD.MM.YYYY')

                } else {
                    date = moment().add(1, 'd').format('DD.MM.YYYY');
                }

                let allUsersWontRasp = await User.createQueryBuilder('u')
                    .where('u.wont_rasp = true')
                    .andWhere('u.vk_group = :vk_group', { vk_group: 'pfek' })
                    .getMany()
                allUsersWontRasp.forEach(async u => {
                    // let date = moment().add(1, 'd').format('DD.MM.YYYY')
                    let { vkid, group } = u
                    const strToSend = await getRasp(date, group)
                    await User.update({ vkid }, { wont_rasp: false })
                    bot.sendMessage(vkid, strToSend)
                })
            }
        } else {
            let c = Conf.create({ param, state: true })
            await Conf.save(c)
            ctx.reply(`${param} Created and SET true`)
        }


    }
})

bot.command('юзеры', async (ctx) => {
    if (ctx.message.from_id === root) {
        let oneDay = moment().format('YYYY-MM-DD 00:00:00')
        let lastOneDay = await User.createQueryBuilder('u')
            .where('u.last_activity >= :oneDay', { oneDay })
            .getCount()
        let threeDays = moment().subtract(2, 'd').format('YYYY-MM-DD 00:00:00')
        let lastThreeDays = await User.createQueryBuilder('u')
            .where('u.last_activity >= :threeDays', { threeDays })
            .getCount()
        let allUsers = await User.createQueryBuilder('u')
            .getCount()
        let str = 'За сегодня: ' + lastOneDay + '\nЗа три дня: ' + lastThreeDays + '\nОбщее кол-во: ' + allUsers
        ctx.reply(str)
    }
})

const updateUserActivity: any = async (vkid) => {
    let u = await User.findOne({ select: ['id'], where: { vkid } })
    if (u) {
        return await User.update(u.id, { last_activity: new Date() })
    }
}

const getAllGroupsNames: any = async () => {
    return new Promise(async (res, rej) => {
        let g = await Rasp.find({ select: ['group_name'], order: { group_name: 'ASC' } })
        let groupArray = []
        g.forEach((g) => {
            let name = g.group_name.split(' ')[0]
            if (groupArray.indexOf(name) === -1) {
                groupArray.push(name)
            }
        })
        res(groupArray)
    })
}
const getAllGroupsNumbers: any = async (group_name) => {
    return new Promise(async (res, rej) => {
        let g = await Rasp.createQueryBuilder('r')
            .select(['r.group_name'])
            .where('LOWER(r.group_name) LIKE LOWER(:group_name)', { group_name: `${group_name}%` })
            .getMany()
        let groupArray = []
        g.forEach((g) => {
            let name = g.group_name.split(' ')[1]
            if (groupArray.indexOf(name) === -1) {
                groupArray.push(name)
            }
        })
        groupArray = groupArray.sort((a, b) => a - b)
        res(groupArray)
    })
}

const getRasp: any = async (date, group_name) => {
    return new Promise(async (res, rej) => {
        let rasp = await Rasp.findOne({ select: ['lessons'], where: { group_name, date } })
        if (rasp) {
            let msg = `${group_name} на ${date}\n\n`
            rasp.lessons.forEach((l, i) => {
                if (l.name === null) {
                    msg += l.number + '.\n'
                    msg += '------------------------------\n'
                } else {
                    msg += `${l.number}. ${l.name}\n`
                    if (l.teacher1 && l.cabinet1) {
                        msg += `${l.teacher1} (${l.cabinet1})\n`
                    }
                    if (l.teacher2 && l.cabinet2) {
                        msg += `${l.teacher2} (${l.cabinet2})\n`
                    }
                    if (i + 1 < rasp.lessons.length) {
                        msg += '------------------------------\n'
                    }
                }
            });
            res({ status: 200, msg })
        } else {
            res({ status: 404, msg: `Расписание ${group_name} на ${date} не найдено :с` })
        }
    })
}

bot.startPolling()




const showMainKeyboard = async (vkid) => {
    let keyboard = Markup.keyboard([
        [
            Markup.button('Расп. на завтра', 'primary'),
        ],
        [
            Markup.button('Расп. на сегодня', 'primary'),
        ],
        [
            Markup.button('Указать группу', 'positive'),
        ],
    ]).oneTime(false)
    bot.sendMessage(vkid, '.', null, keyboard)
}
