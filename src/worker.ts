import xlsx from 'xlsx-populate';
import * as fs from 'fs'
import moment from 'moment';
import { bot, root, Rasp, User } from '.';
import { Rasps } from './entity/rasp.entity';
var http = require('http');


export async function start_parce() {
    return new Promise(async (res, rej) => {
        await clearFolder()
        await sleep(3000)
        await download_rasp()
        await sleep(3000)
        let filesInFolder = await removeAllEmptyFiles()

        filesInFolder.forEach(async date => {
            let rasps = await rasp_parce(date)
            await sleep(1000)
            await InsertUpdateInDB(date, rasps)
        })
        res(true)
    })
}

async function InsertUpdateInDB(date, arrLessons) {
    return new Promise(async (res, rej) => {
        // createConnection().then(async connection => {
        // const connection = getConnection()
        let raspisanie: Rasps
        let saveCount = 0
        for (let group_name in arrLessons) {
            let lessons = arrLessons[group_name]
            switch (group_name) {
                case 'Ф309':
                    group_name = 'Ф 309'
                    break;
                case 'Ф308':
                    group_name = 'Ф 308'
                    break;
            }
            raspisanie = {
                group_name,
                date,
                lessons
            }
            let found = await Rasp.findOne({ where: { date, group_name } })
            if (found) {
                // await Rasp.update(found.id, { lessons })
            } else {
                let created = await Rasp.create(raspisanie)
                await Rasp.save(created)
                saveCount++
            }
            await sleep(200)
        }
        if (saveCount === Object.keys(arrLessons).length) {
            let users = await User.find({ select: ['vkid'], where: { vk_group: 'pfek' } })
            // bot.sendMessage(root, 'У меня появилось расписание на ' + date + ' &#128521;')
            users.forEach(u => {
                bot.sendMessage(u.vkid, 'У меня появилось расписание на ' + date + ' &#128521;')
            })
        }
        // await connection.close()
        res(true)
        // })
    })
}
function clearFolder() {
    return new Promise((res, rej) => {
        fs.readdir('./rasp/', (err, files) => {
            files.forEach(file => {
                fs.unlinkSync('./rasp/' + file)
            })
        })
        res(true)
    })
}

function removeAllEmptyFiles(): Promise<[]> {
    return new Promise(async (res, rej) => {
        fs.readdir('./rasp/', (err, files) => {
            files.forEach(file => {
                let size = fs.statSync('./rasp/' + file).size
                if (size < 10000) {
                    fs.unlinkSync('./rasp/' + file);
                }
            });
        })
        await sleep(3000)

        let filesInFolder: any = []
        fs.readdir('./rasp/', (err, files) => {
            files.forEach(file => {
                file = file.slice(0, -5)
                filesInFolder.push(file)
            })
        })

        await sleep(3000)
        res(filesInFolder)
    })
}
function download_rasp() {
    return new Promise(async (res, rej) => {
        for (let i = 1; i < 10; i++) {
            await sleep(100)
            let date = moment().add(i, 'd').format('DD.MM.YYYY')
            let file = fs.createWriteStream('./rasp/' + date + '.xlsx')
            http.get('http://www.old.fa.ru/fil-spo/perm/student/shedule/Documents/2018-2019%20%D0%B4%D0%BE/%D0%A0%D0%B0%D1%81%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%B8%D0%B5%20%D0%B4%D0%BB%D1%8F%20%D1%81%D1%82%D1%83%D0%B4%D0%B5%D0%BD%D1%82%D0%BE%D0%B2/%D0%A0%D0%B0%D1%81%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%B8%D0%B5%20%D0%BD%D0%B0%20' + date + '%20%D0%BF%D0%BE%20%D0%B3%D1%80%D1%83%D0%BF%D0%BF%D0%B0%D0%BC.xlsx',
                function (response) {
                    response.pipe(file)
                });
        }
        res(true)
    })
}


async function rasp_parce(date) {
    return new Promise((res, rej) => {
        var file_path = './rasp/' + date + ".xlsx";
        let data = {};
        xlsx.fromFileAsync(file_path)
            .then(workbook => {
                let arrayGroupsNames = [];
                let bd = workbook.find('БД');
                let poso = workbook.find('ПОСО');
                let sd = workbook.find('СД');
                let f = workbook.find('Ф');
                let ebu = workbook.find('ЭБУ');
                const groupsNames = [];
                arrayGroupsNames = bd.concat(poso, sd, f, ebu);
                arrayGroupsNames.forEach(element => {
                    let name = element.value();
                    if (name.length < 9 && name !== 'ФДОК') {
                        groupsNames.push(name);
                    }
                });
                arrayGroupsNames = [];

                let lessons = [];
                groupsNames.forEach((group_name) => {
                    let targetValue = workbook.sheet(0).find(group_name);
                    let col = targetValue[0].columnNumber();
                    let row = targetValue[0].rowNumber();
                    let cell_val = workbook.sheet(0).row(row).cell(col).value();
                    let index = workbook.sheet(0).row(row + 1).cell("A").value();
                    let down = null;
                    let lessonsCount = null;
                    let lesson: any = {}
                    lessons = []
                    let prevIndex = 0
                    let lessonsEnd = false
                    let doubleUndefLess = 0
                    for (let i = index; i < 10; i++) {
                        row++;
                        let cellVall = workbook.sheet(0).row(row).cell(col).value()
                        if (groupsNames.indexOf(cellVall) !== -1) {
                            lessonsEnd = true
                        }
                        if (cellVall === 'Нач.англ. 3') {
                            lessonsEnd = true
                        }
                        if (i > 3 && typeof cellVall === 'undefined') {
                            doubleUndefLess++
                        }
                        if (lessonsEnd === false && doubleUndefLess < 2) {
                            let nexIndex = workbook.sheet(0).row(row).cell("A").value();
                            lesson = {}
                            if (typeof nexIndex === 'undefined') {

                                lesson.number = prevIndex
                            } else {
                                lesson.number = nexIndex
                            }
                            lesson.name = cellVall
                            if (typeof lesson.name === 'undefined' || groupsNames.indexOf(lesson.name) !== -1) {
                                lesson.name = null;
                                row++;
                            } else {
                                let time = workbook.sheet(0).row(row).cell("C").value()
                                if (typeof time === 'undefined') {
                                    time = ''
                                } else {
                                    time = ' | ' + workbook.sheet(0).row(row).cell("C").value()
                                }
                                lesson.name += time
                                col++;
                                let cabinet2 = workbook.sheet(0).row(row).cell(col).value();
                                if (typeof cabinet2 !== 'undefined') {
                                    col--;
                                    row++;
                                    let teacher2 = workbook.sheet(0).row(row).cell(col).value();
                                    col += 2;
                                    let teacher1 = workbook.sheet(0).row(row).cell(col).value();
                                    col++;
                                    row--;
                                    let cabinet1 = workbook.sheet(0).row(row).cell(col).value();
                                    lesson.cabinet1 = cabinet1
                                    lesson.teacher1 = teacher1
                                    lesson.cabinet2 = cabinet2
                                    lesson.teacher2 = teacher2
                                    if (typeof cabinet1 === 'undefined' && typeof teacher1 === 'undefined' && typeof cabinet2 !== 'undefined' && typeof teacher2 !== 'undefined') {
                                        lesson.cabinet1 = cabinet2
                                        lesson.teacher1 = teacher2
                                        delete lesson.cabinet2
                                        delete lesson.teacher2
                                    }
                                    if (typeof lesson.cabinet1 === 'undefined' && typeof lesson.teacher1 === 'undefined') {
                                        delete lesson.cabinet1
                                        delete lesson.teacher1
                                    }
                                    if (typeof lesson.cabinet2 === 'undefined' && typeof lesson.teacher2 === 'undefined') {
                                        delete lesson.cabinet2
                                        delete lesson.teacher2
                                    }

                                    col -= 3;
                                    row++;
                                } else {
                                    col--;
                                    row++;
                                    let teacher1_1 = workbook.sheet(0).row(row).cell(col).value();
                                    col += 3;
                                    row--;
                                    let cabinet1_1 = workbook.sheet(0).row(row).cell(col).value();
                                    lesson.cabinet1 = cabinet1_1
                                    lesson.teacher1 = teacher1_1

                                    col -= 3;
                                    row++;
                                }
                            }
                            if (lesson.name !== null) {
                                lessons.push(lesson)
                            }
                            prevIndex = +nexIndex
                        }
                    }
                    data[group_name] = lessons;
                });
            });
        res(data);
    });
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}