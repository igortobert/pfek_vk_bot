import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, Timestamp } from 'typeorm';

@Entity()
export class Config {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ type: 'text', nullable: true })
  param?: string;

  @Column({ type: 'bool', default: false })
  state?: boolean;
}
