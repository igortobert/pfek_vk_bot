// Расписание
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, Timestamp } from 'typeorm';

@Entity()
export class Rasps {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ type: 'text' })
  group_name?: string;

  @Column({ type: 'json', nullable: true })
  lessons?: any;

  @Column({ type: 'text', nullable: true })
  date?: string;
}
