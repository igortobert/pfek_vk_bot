// ПОЛЬЗОВАТЕЛИ
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, Timestamp, UpdateDateColumn } from 'typeorm';

@Entity()
export class Users {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ type: 'int' })
  vkid?: number;

  @Column({ type: 'text', nullable: true })
  first_name?: string;

  @Column({ type: 'text', nullable: true })
  last_name?: string;

  @Column({ type: 'text', nullable: true })
  group?: string;

  @Column({ type: 'text', nullable: true, default: 'pfek' })
  vk_group?: string;

  @Column({ type: 'timestamp', nullable: true })
  last_activity?: Date;

  @Column({ type: 'bool', default: false })
  wont_rasp?: boolean;

  @Column({ type: 'bool', default: false })
  blocked?: boolean;
}
